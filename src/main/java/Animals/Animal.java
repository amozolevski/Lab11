package Animals;

public interface Animal {
    String makeSound();
}
