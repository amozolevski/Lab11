package FiguresTest;

import Figures.Circle;
import Figures.Figure;
import Figures.Square;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ApplicationTest {
    Figure circle;
    Figure square;

    @BeforeMethod
    public void createObj(){
        circle = new Circle(4);
        square = new Square(4);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Length must be positive")
    public void circleRadiusExcTestCase(){
        circle = new Circle(-3);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Length must be positive")
    public void squareLengthExcTestCase(){
        square = new Square(0);
    }

    @Test
    public void getCirclePerimeterTestCase(){
        Assert.assertEquals(8*Math.PI, circle.getPerimeter(), "perimeter isn't correct");
    }

    @Test
    public void getSquarePerimeterTestCase(){
        Assert.assertEquals(16.0, square.getPerimeter(), "perimeter isn't correct");
    }

    @Test
    public void getCircleAreaTestCase(){
        double target = Math.pow(4, 2) * Math.PI;
        Assert.assertEquals(target, circle.getArea(), "area isn't correct");
    }

    @Test
    public void getSquareAreaTestCase(){
        Assert.assertEquals(Math.pow(4, 2), square.getArea(), "area isn't correct");
    }

    @DataProvider(name = "CirclePerimeterTestCase")
    public Object[][] createCirclePerimeterTestCase(){
        return new Object[][]{{10.0},
                            {12.0},
                            {8*Math.PI},
                            {14.0}};
    }

    @Test(dataProvider = "CirclePerimeterTestCase")
    public void circlePerimeterTestCase(double number){
        Assert.assertEquals(number, circle.getPerimeter(), "perimeter isn't correct");
    }
}
